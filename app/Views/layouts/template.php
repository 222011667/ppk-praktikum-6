<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PPK Praktikum 6</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style {csp-style-nonce}>
        body{
          background:#eee;
        }
      .card{
          border:none;

          position:relative;
          overflow:hidden;
          border-radius:8px;
          cursor:pointer;
      }

      .card:before{

          content:"";
          position:absolute;
          left:0;
          top:0;
          width:4px;
          height:100%;
          background-color:#E1BEE7;
          transform:scaleY(1);
          transition:all 0.5s;
          transform-origin: bottom
      }

      .card:after{

          content:"";
          position:absolute;
          left:0;
          top:0;
          width:4px;
          height:100%;
          background-color:#8E24AA;
          transform:scaleY(0);
          transition:all 0.5s;
          transform-origin: bottom
      }

      .card:hover::after{
          transform:scaleY(1);
      }


      .fonts{
          font-size:11px;
      }

      .social-list{
          display:flex;
          list-style:none;
          justify-content:center;
          padding:0;
      }

      .social-list li{
          padding:10px;
          color:#8E24AA;
          font-size:19px;
      }


      .buttons button:nth-child(1){
            border:1px solid #8E24AA !important;
            color:#8E24AA;
            height:40px;
      }

      .buttons button:nth-child(1):hover{
            border:1px solid #8E24AA !important;
            color:#fff;
            height:40px;
            background-color:#8E24AA;
      }

      .buttons button:nth-child(2){
            border:1px solid #8E24AA !important;
            background-color:#8E24AA;
            color:#fff;
              height:40px;
      }


      body{
        background-color: rgba(50, 65, 70, 1.0);
        color: white;
            font-family: 'Ubuntu', sans-serif;
      }
        .card p, .card h5{
            /*color: saddlebrown;*/
        }
        .card{
            background-color: black;
            height: 100%;
        }
        a{
            text-decoration: none;
            color: thistle;
        }
        #rss-tempo{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-cnn{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-republika{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #json-gempa{
            /*background-color: purple;*/
            display: none;
            opacity: 1;
        }
        #rss-tempo h2{

        }

        /*Moving text banner animation*/
        #scroll-container {
          /*border: 3px solid black;*/
          background: rgba(0, 0, 0, 0.1);
          padding: 1em;
          border-radius: 5px;
          overflow: hidden;
        }

        #scroll-text {
            white-space: nowrap;
          /* animation properties */
          -moz-transform: translateX(100%);
          -webkit-transform: translateX(100%);
          transform: translateX(100%);
          
          -moz-animation: my-animation 25s linear infinite;
          -webkit-animation: my-animation 25s linear infinite;
          animation: my-animation 25s linear infinite;
        }

        /* for Firefox */
        @-moz-keyframes my-animation {
          from { -moz-transform: translateX(100%); }
          to { -moz-transform: translateX(-150%); }
        }

        /* for Chrome */
        @-webkit-keyframes my-animation {
          from { -webkit-transform: translateX(100%); }
          to { -webkit-transform: translateX(-150%); }
        }

        @keyframes my-animation {
          from {
            -moz-transform: translateX(100%);
            -webkit-transform: translateX(100%);
            transform: translateX(100%);
          }
          to {
            -moz-transform: translateX(-150%);
            -webkit-transform: translateX(-150%);
            transform: translateX(-150%);
          }
        }
        /*Moving text banner animation*/
        
        /*Fade in Fade out*/
        .fade{
            animation: fadeIn 2s;
        }
        @keyframes fadeIn {
          0% { opacity: 0; }
          100% { opacity: 1; }
        }
        /*Fade in Fade out*/
        .goyang{
            transition: transform .2s; /* Animation */
            user-select: none; /* supported by Chrome and Opera */
           -webkit-user-select: none; /* Safari */
           -khtml-user-select: none; /* Konqueror HTML */
           -moz-user-select: none; /* Firefox */
           -ms-user-select: none;
        }
        .goyang:hover{
            transform: rotate(5deg);
        }
        .accordion-item button{
            background: rgba(0, 50, 50, 0.2);
        }
        nav{
          font-size: 1.5em;
        }

    </style>
</head>
<body class="d-flex flex-column min-vh-100">

<!-- HEADER: MENU + HEROE SECTION -->
<header>
<nav class="navbar navbar-light bg-light navbar-expand-lg">
  <div class="container-fluid">
    <a class="navbar-brand goyang judul" href="/">
     <h2>
       
     PPK - Praktikum 6
     </h2> 
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/profile">Profile</a>
        </li>
        <?php if (auth()->loggedIn()): ?>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Logout</a>
        </li>
        <?php else: ?>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
        <?php endif;?>
      </ul>
    </div>
  </div>
</nav>
</header>

<?=$this->renderSection('content')?>
<!-- FOOTER: DEBUG INFO + COPYRIGHTS -->

<footer class="mt-auto">
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-light" href="https://mdbootstrap.com/">M.Mishbahi</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- SCRIPTS -->

<script>
    function toggleMenu() {
        var menuItems = document.getElementsByClassName('menu-item');
        for (var i = 0; i < menuItems.length; i++) {
            var menuItem = menuItems[i];
            menuItem.classList.toggle("hidden");
        }
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- -->

</body>
</html>
