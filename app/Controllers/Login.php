<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\Events\Events;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\Shield\Controllers\LoginController;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\Shield\Authentication\Authenticators\Session;
class Login extends LoginController
{
    protected $helpers = ['setting'];
    private $userModel = NULL;
    private $user = NULL;
    private $google_client = NULL;
    function __construct()
    {
        require_once __DIR__ . '/../../vendor/autoload.php';
        $this->userModel = new UserModel(); 
        $this->google_client = new \Google\Client(); 
        // $this->google_client->setClientId(getenv('OAuth2.clientID'));
        // $this->google_client->setClientSecret(getenv('OAuth2.clientSecret'));
        $this->google_client->setAuthConfig(__DIR__ . '/../../client_secret_1052507275748-oheougvcdce6us778pjlctgeecr8dmnp.apps.googleusercontent.com.json');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
        $this->google_client->setRedirectUri('http://localhost/dashboard/');
        // $this->google_client->addScope(\Google\Service\Gmail::GMAIL_READONLY);
    }
    public function loginView()
    {
        if (auth()->loggedIn()) {
            return redirect()->to(config('Auth')->loginRedirect());
        }

        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        // If an action has been defined, start it up.
        if ($authenticator->hasAction()) {
            return redirect()->route('auth-action-show');
        }

        $data['google_button'] = "<a href='".$this->google_client->createAuthUrl()."'><img src='https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png' /></a>";
        return view('login', $data);
    }

    public function loginAction(): RedirectResponse
    {
        // Validate here first, since some things,
        // like the password, can only be validated properly here.
        $rules = $this->getValidationRules();

        if (! $this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        $credentials             = $this->request->getPost(setting('Auth.validFields'));
        $credentials             = array_filter($credentials);
        $credentials['password'] = $this->request->getPost('password');
        $remember                = (bool) $this->request->getPost('remember');

        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        // Attempt to login
        $result = $authenticator->remember($remember)->attempt($credentials);
        if (! $result->isOK()) {
            return redirect()->route('login')->withInput()->with('error', $result->reason());
        }

        /** @var Session $authenticator */
        $authenticator = auth('session')->getAuthenticator();

        // If an action has been defined for login, start it up.
        if ($authenticator->hasAction()) {
            return redirect()->route('auth-action-show')->withCookies();
        }

        return redirect()->to(config('Auth')->loginRedirect())->withCookies();
    }

    public function login_google() {
            $token = $this->google_client->fetchAccessTokenWithAuthCode($this->request->getVar('code'));
            if (!isset($token['error'])) {
                $this->google_client->setAccessToken($token['access_token']);
                session()->set('AccessToken', $token['access_token']);
                $google_service = new \Google\Service\Oauth2($this->google_client);
                $data = $google_service->userinfo->get();
                $user_data = array();
                // echo "<pre>";
                // print_r($data);
                // echo "</pre>";
                if ($this->userModel->isAlreadyRegister($data['id'])) {
                    $user_data = [
                        "username" => $data['givenName'].''.$data['familyName'],
                        "email" => $data['email'],
                        "avatar" => $data['picture'],
                        "first_name" => $data['givenName'],
                        "last_name" => $data['familyName'],
                        "updated_at" => date('Y-m-d H:i:s'),
                    ];
                    
                    // $password = $this->userModel->getUserData($data['email']);
                    $this->userModel->updateUserData($data['id'], $user_data);
                    
                    $authenticator = auth('session')->getAuthenticator();
                    $remember = (bool) $this->request->getPost('remember');
                    // Attempt to login
                    $result = $authenticator->remember($remember)->attempt(['email' => $data['email'], 'password' => $data['id']]);
                    if (! $result->isOK()) {
                        return redirect()->route('login')->withInput()->with('error', $result->reason());
                    }
                    $authenticator = auth('session')->getAuthenticator();
                    if ($authenticator->hasAction()) {
                        return redirect()->route('auth-action-show')->withCookies(); 
                    }
                    return redirect()->to(config('Auth')->loginRedirect())->withCookies();
                } else {
                    $user_data = new User([
                        "username" => $data['givenName'].''.$data['familyName'],
                        "oauth_id" => $data['id'],
                        "password" => $data['id'],
                        "email" => $data['email'],
                        "avatar" => $data['picture'],
                        "first_name" => $data['givenName'],
                        "last_name" => $data['familyName'],
                        "created_at" => date('Y-m-d H:i:s'),
                        "updated_at" => date('Y-m-d H:i:s'),
                    ]);
                    $this->userModel->save($user_data);
                    $user = $this->userModel->find($this->userModel->getInsertID());
                    $this->userModel->addToDefaultGroup($user);
                    Events::trigger('register', $user);
                    $authenticator = auth('session')->getAuthenticator();
                    $authenticator->startLogin($user);

                    $hasAction = $authenticator->startUpAction('register', $user);
                    if ($hasAction) {
                        return redirect()->to('auth/a/show');
                    }

                    $authenticator->activateUser($user);
                    $authenticator->completeLogin($user);

                    return redirect()->to(config('Auth')->registerRedirect())->with('message', lang('Auth.registerSuccess'));
                }
                session()->set('LoggedUserData', $user_data); 
            } else {
                session()->set("error", $token['error']);
                return redirect()->route('/register');
            }

            return redirect()->to(config('Auth')->loginRedirect())->withCookies();
    }

    public function logoutAction(): RedirectResponse
    {
        /** @var Session $authenticator */
        session()->remove('LoggedUserData');
        session()->remove('AccessToken');
        auth()->logout();
        return redirect()->to(config('Auth')->logoutRedirect())->with('message', lang('Auth.successLogout'));
    }
}
